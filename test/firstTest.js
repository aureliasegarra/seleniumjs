const {Builder, Browser, By, Key, until} = require('selenium-webdriver');

(async function example () {
    let driver = await new Builder().forBrowser(Browser.SAFARI).build();
    try {
        await driver.navigate().to('https://www.google.com/');
        await driver.sleep(5000);
        await driver.findElement(By.id('vc3jof')).click();
        await driver.findElement(By.className('Ge0Aub')).click();
        await driver.findElement(By.id('L2AGLb')).click();
        await driver.wait,(until.titleIs('webdriver - Google Search'), 1000);
    } finally {
        await driver.quit();
    }
})();